
import microRedBook from '../../components/MicroRedBook/RedBookIndex.vue'
import redBookRank from '../../components/MicroRedBook/RedBookRank.vue'

const routes = [
    {
        path: '',
        component: microRedBook,
        children: [
            {
                path: '/redBook/rank',
                component: redBookRank
            }
        ]
    }
]

export default routes