// router.js
import { createRouter, createWebHistory } from 'vue-router';
import MicroRedBook from './module/micro-red-book'

const routes = [
    ...MicroRedBook
]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;